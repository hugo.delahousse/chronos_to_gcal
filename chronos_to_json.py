"""
This module parses ugly html from chronos to return events as
beautiful json objects with metadata used by the google calendar api
"""

import os
import re
from datetime import datetime, timedelta
import requests
import json
from bs4 import BeautifulSoup

CLASS_NAME = os.environ.get('CLASS_NAME', 'BING')

CHRONOS_URL = "http://chronos.epita.net/"
CHRONOS_TREE_URL = CHRONOS_URL + "ade/standard/gui/tree.jsp"
CHRONOS_WEEKS_URL = CHRONOS_URL + "ade/custom/modules/plannings/pianoWeeks.jsp"
CHRONOS_BOUNDS_URL = CHRONOS_URL + "ade/custom/modules/plannings/bounds.jsp"
CHRONOS_PLANNING_URL = CHRONOS_URL + "ade/custom/modules/plannings/info.jsp"

DEFAULT_REMINDER = {
    'useDefault': False,
    'overrides': [
        {'method': 'popup', 'minutes': 5},
    ]
}

ACTIVITY_COLORS = {
    "Mathématiques":"4",
    "Anglais général":"6",
    "Anglais technique":"6",
    "Architecture":"8",
    "Physique":"10",
    "Electronique":"5",
    "Algorithmique":"1",
    "Techniques d'expression":"3",
    "Programmation":"2"
}

def get_current_week(sess=None):
    if not sess:
        sess = requests.session()
        sess.get(CHRONOS_URL)
    r = sess.get(CHRONOS_WEEKS_URL)
    if (r.status_code != 200):
        return None
    soup = BeautifulSoup(r.text, 'lxml')
    try:
        selected = soup.find(class_='pianoselected')
        area_href = selected.find('area').attrs.get('href')
        week = re.sub("[^\d]", "", area_href)
        return int(week)
    except:
        return None


def get_tree(className, sess=None, week=None):
    if not sess:
        sess = requests.session()
        sess.get(CHRONOS_URL)
    if week:
        current_week = get_current_week(sess)
        sess.get(CHRONOS_BOUNDS_URL,
                 params={"reset": "false", "week": current_week})
        for i in range(0, week):
            sess.get(CHRONOS_BOUNDS_URL,
                     params={"reset": "false", "week": current_week + i})
    sess.get(CHRONOS_TREE_URL, params={"search": className, "reset": "false"})
    html = sess.get(CHRONOS_PLANNING_URL).text
    return html

def tree_to_json(html):
    soup = BeautifulSoup(html, 'lxml')
    tags = []
    final_objects = []

    rows = soup.find_all('tr')
    for cell in rows[0].find_all('th'):
        tags.append(cell.text)

    for i in range(1, len(rows)):
        obj = {}
        cells = rows[i].find_all('td')
        for j, _ in enumerate(cells):
            cell = cells[j]
            obj[tags[j]] = cell.text

        final_objects.append(obj)

    return final_objects

def format_json(events):
    for event in events:
        day, month, year = event["Date"].split("/")
        hour, minute = event["Heure"].split("h")
        event["Date"] = datetime(int(year),
                                 int(month),
                                 int(day),
                                 int(hour),
                                 int(minute))
        split_duration = event["Durée"].split("h")
        duration = {'hours': int(split_duration[0]), 'minutes': 0}
        if split_duration[1]:
            duration['minutes'] = int(split_duration[1].replace('min', ''))
        event["Durée"] = timedelta(**duration)
    return events

def merge_events(events):
    result = []

    for event in events:
        if result:
            last_event = result[-1]
            if (event["Activité"] == last_event["Activité"]
                    and last_event["Date"] + last_event["Durée"] == event["Date"]
                    and event["Salles"] == last_event["Salles"]):
                result[-1]["Durée"] += event["Durée"]
            else:
                result.append(event)
        else:
            result.append(event)

    return result




def json_to_events(events):
    result = []
    for obj in events:
        event = {}
        event["summary"] = obj["Activité"]
        event["location"] = obj["Salles"]
        if obj.get("Formateurs"):
            event["description"] = "Formateurs: " + obj.get("Formateurs")
        start = obj["Date"]
        end = start + obj["Durée"]

        event["reminder"] = DEFAULT_REMINDER

        event["start"] = {"dateTime": start.isoformat(),
                          "timeZone": "Europe/Paris"}
        event["end"] = {"dateTime": end.isoformat(),
                        "timeZone": "Europe/Paris"}

        event["colorId"] = ACTIVITY_COLORS.get(obj["Activité"], "7")

        result.append(event)
    return result

def get_gcalendar_events(class_name, week=None):
    return json_to_events(
        merge_events(
            format_json(
                tree_to_json(
                    get_tree(class_name, None, week)
                    )
                )
            )
        )


if __name__ == '__main__':
    print(json.dumps(get_gcalendar_events(CLASS_NAME, 4), indent=4))
