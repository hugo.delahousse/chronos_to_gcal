==========================
Chronos to Google Calendar
==========================

This project is meant to provide to epita students a way to
have a proper weekly schedule.

Students or class deleguates can create a google calendar, retreive a
CALENDAR_ID, and use the `sync_event.py` file to synchronise events from
chronos to the new calendar. They can then share this calendar with their
classmates.

Setup
=====

First of all, read the quickstart on the python google calendar api
`here <https://developers.google.com/google-apps/calendar/quickstart/python>`_.

Install the requirements with :code:`pip install -r requirements.py`

You need to create a `secret.py` file containing 4 constants.

* :code:`SCOPES` should be set to `https://www.googleapis.com/auth/calendar`
* :code:`CLIENT_SECRET_FILE` should be set to the name of the
  `client_secret.json file`
* :code:`APPLICATION NAME` should be set to the name of your application
* :code:`CALENDAR_ID` should be set to the ID of the calendar to edit.

Usage
=====

.. code::

  python3 sync_events.py

