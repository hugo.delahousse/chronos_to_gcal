"""
This module is used to get credentials for the google calendar API
"""
import os
# from oauth2client import client, tools
import oauth2client

from secrets import SCOPES, CLIENT_SECRET_FILE, APPLICATION_NAME

CREDENTIALS_DIR = os.path.join(os.path.expanduser('~'), '.credentials')

if not os.path.exists(CREDENTIALS_DIR):
    os.makedirs(CREDENTIALS_DIR)


CREDENTIAL_PATH = os.path.join(CREDENTIALS_DIR,
                               'calendar-python-quickstart.json')

def get_credentials():
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
        Credentials, the obtained credential.
    """

    store = oauth2client.file.Storage(CREDENTIAL_PATH)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = oauth2client.client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        print('Storing credentials to ' + CREDENTIAL_PATH)
    return credentials
