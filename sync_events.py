import os
import logging
from datetime import datetime, timedelta

import httplib2
import pytz
from apiclient import discovery
from api_credentials import get_credentials
from chronos_to_json import get_gcalendar_events
from secrets import CALENDAR_ID

CLASS_NAME = os.environ.get('CLASS_NAME', 'BING')

def log_batch_result(_, __, exception):
    "Callback for batch requests"
    if exception:
        logging.warning("Batch failed")
    else:
        logging.info("Batch succeded")

def clear_next_events(service, batch):
    now = datetime.now(tz=pytz.timezone('Europe/Paris')) + timedelta(hours=1)
    next_events = service.events().list(
        calendarId=CALENDAR_ID,
        timeMin=now.isoformat(),
        timeMax=(now + timedelta(days=45)).isoformat(),
        singleEvents=True,
        orderBy='startTime').execute()
    to_delete = next_events.get('items')
    if not to_delete:
        logging.info('No upcoming events to delete.')
    else:
        for event in to_delete:
            batch.add(service.events().delete(
                calendarId=CALENDAR_ID,
                eventId=event['id']))

def add_next_events(service, batch):
    next_events = get_gcalendar_events(CLASS_NAME, 4)
    now = datetime.now(tz=pytz.timezone('Europe/Paris'))
    if not next_events:
        logging.warning('No future events')

    for event in next_events:
        event_start = datetime.strptime(event['start']['dateTime'],
                                        '%Y-%m-%dT%H:%M:%S')
        event_start = event_start.replace(tzinfo=pytz.timezone('Europe/Paris'))
        if event_start > now:
            batch.add(service.events().insert(
                calendarId=CALENDAR_ID,
                body=event))

def sync_events():
    credentials = get_credentials()
    service = discovery.build('calendar', 'v3',
                              http=credentials.authorize(httplib2.Http()))
    batch = service.new_batch_http_request(callback=log_batch_result)
    clear_next_events(service, batch)
    add_next_events(service, batch)
    batch.execute(http=httplib2.Http())

if __name__ == "__main__":
    sync_events()
